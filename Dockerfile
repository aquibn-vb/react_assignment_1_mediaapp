FROM node:15.13-alpine
WORKDIR /react-tracks
ENV PATH ="./node_modules/.bin:$PATH"
COPY package.json ./
COPY package-lock.json ./
RUN npm install
COPY . .
RUN npm run build
CMD ["npm","start"]